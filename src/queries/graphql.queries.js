import gql from "graphql-tag";

export const fundsQuery = gql`
  {
    funds {
      id
      name
      description
      fundAmount
      fundRaiserId
      fundRaiser {
        id
        name
        age
        gender
      }
    }
  }
`;

export const fundRaisersQuery = gql`
  {
    fundRaisers {
      id
      name
      age
      gender
      funds {
        id
        name
        description
        fundAmount
      }
    }
  }
`;

export const oneFundQuery = gql`
  query($id: ID) {
    fund(id: $id) {
      id
      name
      description
      fundAmount
      fundRaiser {
        id
        name
        age
        gender
      }
    }
  }
`;
export const oneFundRaiserQuery = gql`
  query($id: ID) {
    fundRaiser(id: $id) {
      id
      name
      age
      gender
      funds {
        id
        name
        description
        fundAmount
        fundRaiserId
      }
    }
  }
`;

export const addFundMutation = gql`
  mutation(
    $name: String!
    $description: String!
    $fundAmount: Int!
    $fundRaiserId: String!
  ) {
    addFund(
      name: $name
      description: $description
      fundAmount: $fundAmount
      fundRaiserId: $fundRaiserId
    ) {
      name
    }
  }
`;
export const addFundRaiserMutation = gql`
  mutation($name: String!, $age: Int!, $gender: String!) {
    addFundRaiser(name: $name, age: $age, gender: $gender) {
      name
    }
  }
`;
