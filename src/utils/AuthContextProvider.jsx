import React, { useState } from "react";
export const AuthContext = React.createContext();

function AuthContextProvider(props) {
  const [LoggedIn, setLoggedIn] = useState(false);

  return (
    <AuthContext.Provider value={{ LoggedIn, setLoggedIn }}>
      {props.children}
    </AuthContext.Provider>
  );
}
export default AuthContextProvider;
