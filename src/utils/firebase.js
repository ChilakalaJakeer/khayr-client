import app from "firebase/app";
import "firebase/auth";
import "firebase/firebase-firestore";

const config = {
  apiKey: "AIzaSyDukaxX0i91HMrY42i8l3ITmA0alFsbYeo",
  authDomain: "auth-for-crud.firebaseapp.com",
  databaseURL: "https://auth-for-crud.firebaseio.com",
  projectId: "auth-for-crud",
  storageBucket: "auth-for-crud.appspot.com",
  messagingSenderId: "670845504586",
  appId: "1:670845504586:web:2fd50f308c979c1576eab3",
  measurementId: "G-8HRNGZ6QBT"
};

class firebase {
  constructor() {
    app.initializeApp(config);
    this.auth = app.auth();
    this.db = app.firestore();
  }
  login(email, password) {
    return this.auth.signInWithEmailAndPassword(email, password);
  }
  logout() {
    return this.auth.signOut();
  }
  async register(name, email, password) {
    await this.auth.createUserWithEmailAndPassword(email, password);
    return this.auth.currentUser.updateProfile({
      displayName: name
    });
  }

  isInitialized() {
    return new Promise(resolve => {
      this.auth.onAuthStateChanged(resolve);
    });
  }

  getCurrentUsername() {
    return this.auth.currentUser && this.auth.currentUser.displayName;
  }
}
export default new firebase();
