import React, { useContext } from "react";
import { BrowserRouter as Router, Route, Redirect } from "react-router-dom";

import Navbar from "../components/Navbar";
import Login from "../components/page.auth/Login";
import Register from "../components/page.auth/Register";
import Home from "../components/page.home/Home";
import Detail from "../components/page.detail/Detail";
import Form from "../components/page.form/Form";
import { AuthContext } from "./AuthContextProvider";
import CreateFundRaiser from "../components/page.profile/CreateFundRaiser";
import Copyrights from "../components/Copyrights";

const PrivateRoute = ({ component: Component, ...rest }) => {
  const { LoggedIn } = useContext(AuthContext);
  return (
    <Route
      {...rest}
      render={props =>
        LoggedIn ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: props.location }
            }}
          />
        )
      }
    />
  );
};

export default function Routes(props) {
  return (
    <Router>
      <Navbar />
      <Route exact path="/" component={Home} />

      <Route path="/login" component={Login} />
      <Route path="/register" component={Register} />

      <Route path="/detail/:id" component={Detail} />
      <PrivateRoute path="/form" component={Form} />
      <PrivateRoute path="/profile" component={CreateFundRaiser} />
      <Copyrights />
    </Router>
  );
}
