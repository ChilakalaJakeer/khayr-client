import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import Grid from "@material-ui/core/Grid";
import Fab from "@material-ui/core/Fab";
import ListAltIcon from "@material-ui/icons/ListAlt";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import makeStyles from "@material-ui/core/styles/makeStyles";

import { useQuery, useMutation } from "@apollo/react-hooks";
import {
  fundRaisersQuery,
  addFundMutation
} from "../../queries/graphql.queries";
import RouteLink from "../elements/RouteLink";

const useStyles = makeStyles(theme => ({
  main: {
    width: "auto",
    display: "block", // Fix IE 11 issue.
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    [theme.breakpoints.up(400 + theme.spacing(3))]: {
      width: "80%",
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing(2)}px ${theme.spacing(3)}px ${theme.spacing(
      3
    )}px`,
    boxShadow: "5px 5px 10px"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  cancel: {
    marginTop: theme.spacing(5)
  },
  submit: {
    backgroundColor: theme.palette.success.main,
    color: theme.palette.primary.contrastText,
    marginTop: theme.spacing(5),
    "&:hover": {
      backgroundColor: theme.palette.success.main,
      opacity: 0.9
    }
  }
}));

export default function Form(props) {
  const classes = useStyles();

  const [Title, setTitle] = useState("");
  const [Content, setContent] = useState("");
  const [FundAmount, setFundAmount] = useState("");
  const [FundRaiser, setFundRaiser] = useState("");

  const { loading: fundRaisersLoading, data: fundRaisersData } = useQuery(
    fundRaisersQuery
  );
  const [addFund, { data }] = useMutation(addFundMutation);

  const submit = e => {
    e.preventDefault();
    Title &&
      Content &&
      FundRaiser &&
      addFund({
        variables: {
          name: Title,
          description: Content,
          fundAmount: FundAmount,
          fundRaiserId: FundRaiser
        }
      });
    if (data) {
      return (window.location = "/");
    }
    return null;
  };
  return (
    <main className={classes.main}>
      <Paper className={classes.paper}>
        <Avatar className={classes.avatar}>
          <ListAltIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Form
        </Typography>
        <form
          className={classes.form}
          onSubmit={e => e.preventDefault() && false}
        >
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="title">Title</InputLabel>
            <Input
              id="title"
              name="title"
              autoComplete="off"
              autoFocus
              value={Title}
              onChange={e => setTitle(e.target.value)}
            />
          </FormControl>
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="content">Content</InputLabel>
            <Input
              id="content"
              name="content"
              autoComplete="off"
              multiline
              rowsMax="5"
              value={Content}
              onChange={e => setContent(e.target.value)}
            />
          </FormControl>
          <TextField
            required
            fullWidth
            label="Content2"
            id="content2"
            name="content2"
            autoComplete="off"
            multiline
            rows="5"
            value={Content}
            onChange={e => setContent(e.target.value)}
          />
          <TextField
            required
            fullWidth
            type="number"
            label="Fund Amount"
            id="fundAmount"
            name="fundAmount"
            autoComplete="off"
            value={FundAmount}
            onChange={e => setFundAmount(parseInt(e.target.value))}
          />
          <FormControl required fullWidth>
            <InputLabel>FundRaiser</InputLabel>
            <Select name="user" onChange={e => setFundRaiser(e.target.value)}>
              <MenuItem value="" disabled>
                Please select FundRaiser name
              </MenuItem>
              {fundRaisersLoading ? (
                <MenuItem value="loading">Loading...</MenuItem>
              ) : (
                fundRaisersData.fundRaisers.map(fundRaiser => (
                  <MenuItem value={fundRaiser.id} key={fundRaiser.id}>
                    {fundRaiser.name}
                  </MenuItem>
                ))
              )}
              <MenuItem value="addNewUser">addNewUser</MenuItem>
            </Select>
          </FormControl>
          {FundRaiser === "addNewUser" && (
            <RouteLink
              to={{
                pathname: "/profile",
                state: { from: props.location }
              }}
            >
              <Fab>
                <PersonAddIcon />
              </Fab>
            </RouteLink>
          )}
          <Grid container justify="center" spacing={3}>
            <Grid item md={5}>
              <Button
                type="submit"
                variant="contained"
                color="secondary"
                fullWidth
                // onClick={}
                className={classes.cancel}
              >
                Cancel
              </Button>
            </Grid>
            <Grid item md={5}>
              <Button
                type="submit"
                variant="contained"
                fullWidth
                onClick={submit}
                className={classes.submit}
              >
                Submit
              </Button>
            </Grid>
          </Grid>
        </form>
      </Paper>
    </main>
  );
}
