import React from "react";
import { Grid, Typography, Paper, makeStyles } from "@material-ui/core";
import ShareWidget from "../elements/ShareWidget";

import { useQuery } from "@apollo/react-hooks";
import { oneFundQuery } from "../../queries/graphql.queries";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: theme.spacing(3)
    // position: "relative"
  },
  root2: {
    minWidth: "80%" //change
  },
  paper: {
    background: "wheat"
  },
  shareButton: {
    position: "fixed",
    top: theme.spacing(19),
    right: theme.spacing(0),
    borderRadius: `${theme.spacing(3)}px ${theme.spacing(0)}px ${theme.spacing(
      0
    )}px ${theme.spacing(3)}px`
  }
}));
export default function Detail(props) {
  const classes = useStyles();
  const fundId = props.match.params.id;

  const { loading: fundLoading, data: fundData } = useQuery(oneFundQuery, {
    variables: { id: fundId }
  });

  if (fundLoading) {
    return <div>Loading ...</div>;
  }
  const { name, description, fundAmount, fundRaiser } = fundData.fund;
  return (
    <div className={classes.root}>
      <ShareWidget classes={classes} />
      <Grid container justify="center">
        <Grid item md={10} className={classes.root2}>
          <Paper className={classes.paper}>
            <Grid container justify="center" spacing={3}>
              <Grid item md={10}>
                <Typography component="h1" variant="h5">
                  {name}
                  <span style={{ float: "right" }}>{fundAmount}</span>
                </Typography>
              </Grid>
            </Grid>
            <Grid container justify="center" spacing={3}>
              <Grid item md={10}>
                <Typography component="article" variant="body1">
                  {fundRaiser.name}
                  <br />
                  {description}
                </Typography>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}
