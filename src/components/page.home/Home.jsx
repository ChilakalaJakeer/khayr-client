import React from "react";
import DataCard from "../elements/DataCard";
import { useQuery } from "@apollo/react-hooks";
import { fundsQuery } from "../../queries/graphql.queries";
import Grid from "@material-ui/core/Grid";

export default function Home(props) {
  const { loading: fundsLoading, data: fundsData } = useQuery(fundsQuery);

  if (fundsLoading) {
    return <div>Loading ...</div>;
  } else {
    return (
      <div>
        <Grid container justify="center" spacing={2}>
          {fundsData.funds.map(fund => (
            <Grid item sm={5} md={4} lg={3} key={fund.id}>
              <DataCard fund={fund} />
            </Grid>
          ))}
        </Grid>
      </div>
    );
  }
}
