import React, { useContext } from "react";

import IconButton from "@material-ui/core/IconButton";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import AccountCircle from "@material-ui/icons/AccountCircle";
import MoreIcon from "@material-ui/icons/MoreVert";
import HomeIcon from "@material-ui/icons/Home";
import ListAltIcon from "@material-ui/icons/ListAlt";
import RouteLink from "./elements/RouteLink";
import { Button } from "@material-ui/core";
import { AuthContext } from "../utils/AuthContextProvider";

export default function NavItems(props) {
  const [
    classes,
    menuId,
    handleMobileMenuClose,
    handleMobileMenuOpen,
    mobileMoreAnchorEl,
    handleProfileMenuOpen,
    mobileMenuId,
    isMobileMenuOpen
  ] = props.pass;
  const { LoggedIn } = useContext(AuthContext);
  return (
    <>
      <div className={classes.sectionDesktop}>
        <RouteLink to="/">
          <IconButton className={classes.icon}>
            <HomeIcon />
          </IconButton>
        </RouteLink>

        <RouteLink to="/form">
          <IconButton className={classes.icon}>
            <ListAltIcon />
          </IconButton>
        </RouteLink>
      </div>

      <div className={classes.sectionMobile}>
        <IconButton
          aria-label="show more"
          aria-controls={mobileMenuId}
          aria-haspopup="true"
          onClick={handleMobileMenuOpen}
          color="inherit"
        >
          <MoreIcon />
        </IconButton>
      </div>
      {LoggedIn ? (
        <IconButton
          edge="end"
          aria-label="account of current user"
          aria-controls={menuId}
          aria-haspopup="true"
          onClick={handleProfileMenuOpen}
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
      ) : (
        <RouteLink to="/login">
          <Button className={classes.login}>Login</Button>
        </RouteLink>
      )}
      {/* {renderMobileMenu} */}

      <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
        id={mobileMenuId}
        keepMounted
        transformOrigin={{ vertical: "top", horizontal: "right" }}
        open={isMobileMenuOpen}
        onClose={handleMobileMenuClose}
      >
        <MenuItem>
          <RouteLink to="/">
            <IconButton color="inherit">
              <HomeIcon />
            </IconButton>
            <p> Home</p>
          </RouteLink>
        </MenuItem>

        <MenuItem>
          <RouteLink to="/form">
            <IconButton color="inherit">
              <ListAltIcon />
            </IconButton>
            <p> Form</p>{" "}
          </RouteLink>
        </MenuItem>
      </Menu>
    </>
  );
}
