import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import FavoriteIcon from "@material-ui/icons/Favorite";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import RouteLink from "./RouteLink";
import ShareWidget from "./ShareWidget";

const useStyles = makeStyles(theme => ({
  card: {
    maxWidth: 270,
    margin: theme.spacing(3),
    background: "wheat",
    display: "inline-block"
  },
  contentContainer: {
    height: 230,
    overflow: "hidden"
  },

  actionIcon: {
    fontSize: "15px",
    marginRight: theme.spacing(1)
  }
}));

export default function DataCard(props) {
  const classes = useStyles();
  const { name, description, id } = props.fund;
  const dummyData = {
    media:
      "https://images.unsplash.com/photo-1569429210811-641dced2167e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60",
    title: name,
    content: description
  };

  return (
    <Card className={classes.card}>
      <RouteLink to={"/detail/" + id}>
        <CardActionArea>
          <CardMedia
            component="img"
            alt="Texture"
            height="200"
            image={dummyData.media}
            title="Texture"
          />
          <CardContent className={classes.contentContainer}>
            <Typography gutterBottom variant="h5" component="h2">
              {dummyData.title}
            </Typography>
            <Typography
              variant="body2"
              color="textSecondary"
              component="p"
              className={classes.content}
            >
              {dummyData.content}
            </Typography>
          </CardContent>
        </CardActionArea>
      </RouteLink>
      <CardActions>
        <ShareWidget card classes={classes} />

        <Button size="small" color="secondary">
          <FavoriteIcon className={classes.actionIcon} /> Donate
        </Button>
        <Button size="small" color="primary">
          <RouteLink to={"/detail/" + id}>
            <MoreHorizIcon className={classes.actionIcon} /> More
          </RouteLink>
        </Button>
      </CardActions>
    </Card>
  );
}
