import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";

const useStyles = makeStyles(theme => ({
  root: {
    textDecoration: "none",
    color: theme.palette.common.black,

    // paddingRight: theme.spacing(5),
    "&:hover": {
      //   background: theme.palette.action.hover
    },
    "&>*": {
      display: "inline-block"
    }
  }
}));
export default function RouteLink(props) {
  const classes = useStyles();
  return (
    <Link to={props.to} className={classes.root}>
      {props.children}
    </Link>
  );
}
