import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Popover from "@material-ui/core/Popover";
import Button from "@material-ui/core/Button";
import ShareIcon from "@material-ui/icons/Share";

import {
  EmailShareButton,
  FacebookShareButton,
  LinkedinShareButton,
  TwitterShareButton,
  WhatsappShareButton
} from "react-share";
import {
  EmailIcon,
  FacebookIcon,
  LinkedinIcon,
  TwitterIcon,
  WhatsappIcon
} from "react-share";

const useStyles = makeStyles(theme => ({
  popper: {
    width: theme.spacing(10),
    margin: theme.spacing(0)
  },
  sharebutton: {
    "&:focus": {
      outline: "none"
    }
  },
  shareicon: {
    margin: theme.spacing(0)
  }
}));

export default function ShareWidget(props) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;

  return (
    <div>
      {props.card ? (
        <Button
          aria-describedby={id}
          size="small"
          color="primary"
          onClick={handleClick}
        >
          <ShareIcon className={props.classes.actionIcon} /> Share
        </Button>
      ) : (
        <Button
          color="primary"
          onClick={handleClick}
          className={props.classes.shareButton}
          variant="contained"
        >
          <ShareIcon />
        </Button>
      )}
      {props.card ? (
        <Popover
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: "top",
            horizontal: "center"
          }}
          transformOrigin={{
            vertical: "bottom",
            horizontal: "center"
          }}
          className={classes.popper}
        >
          <EmailShareButton className={classes.sharebutton}>
            <EmailIcon size={40} className={classes.shareicon} />
          </EmailShareButton>
          <TwitterShareButton url="#" className={classes.sharebutton}>
            <TwitterIcon size={40} className={classes.shareicon} />
          </TwitterShareButton>

          <WhatsappShareButton url="#" className={classes.sharebutton}>
            <WhatsappIcon size={40} className={classes.shareicon} />
          </WhatsappShareButton>
          <FacebookShareButton url="#" className={classes.sharebutton}>
            <FacebookIcon size={40} className={classes.shareicon} />
          </FacebookShareButton>

          <LinkedinShareButton url="#" className={classes.sharebutton}>
            <LinkedinIcon size={40} className={classes.shareicon} />
          </LinkedinShareButton>

          {/*   <InstapaperShareButton url="#" className={classes.sharebutton}>
          <InstapaperIcon size={40} className={classes.shareicon} />
        </InstapaperShareButton>
         <PocketShareButton url="#" className={classes.sharebutton}>
          <PocketIcon size={40} className={classes.shareicon} />
        </PocketShareButton>

        <RedditShareButton url="#" className={classes.sharebutton}>
          <RedditIcon size={40} className={classes.shareicon} />
        </RedditShareButton>

        <TelegramShareButton url="#" className={classes.sharebutton}>
          <TelegramIcon size={40} className={classes.shareicon} />
        </TelegramShareButton> */}
        </Popover>
      ) : (
        <Popover
          id={id}
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: "center",
            horizontal: "right"
          }}
          transformOrigin={{
            vertical: "center",
            horizontal: "right"
          }}
          //   className={}
        >
          <EmailShareButton className={classes.sharebutton}>
            <EmailIcon size={40} className={classes.shareicon} />
          </EmailShareButton>
          <TwitterShareButton url="#" className={classes.sharebutton}>
            <TwitterIcon size={40} className={classes.shareicon} />
          </TwitterShareButton>

          <WhatsappShareButton url="#" className={classes.sharebutton}>
            <WhatsappIcon size={40} className={classes.shareicon} />
          </WhatsappShareButton>
          <FacebookShareButton url="#" className={classes.sharebutton}>
            <FacebookIcon size={40} className={classes.shareicon} />
          </FacebookShareButton>

          <LinkedinShareButton url="#" className={classes.sharebutton}>
            <LinkedinIcon size={40} className={classes.shareicon} />
          </LinkedinShareButton>

          {/*   <InstapaperShareButton url="#" className={classes.sharebutton}>
      <InstapaperIcon size={40} className={classes.shareicon} />
    </InstapaperShareButton>
     <PocketShareButton url="#" className={classes.sharebutton}>
      <PocketIcon size={40} className={classes.shareicon} />
    </PocketShareButton>

    <RedditShareButton url="#" className={classes.sharebutton}>
      <RedditIcon size={40} className={classes.shareicon} />
    </RedditShareButton>

    <TelegramShareButton url="#" className={classes.sharebutton}>
      <TelegramIcon size={40} className={classes.shareicon} />
    </TelegramShareButton> */}
        </Popover>
      )}
    </div>
  );
}
