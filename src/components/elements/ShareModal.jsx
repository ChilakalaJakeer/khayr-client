import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import Backdrop from "@material-ui/core/Backdrop";
import Fade from "@material-ui/core/Fade";
import Button from "@material-ui/core/Button";
import ShareIcon from "@material-ui/icons/Share";

import {
  EmailShareButton,
  FacebookShareButton,
  InstapaperShareButton,
  LinkedinShareButton,
  PocketShareButton,
  RedditShareButton,
  TelegramShareButton,
  TwitterShareButton,
  WhatsappShareButton
} from "react-share";
import {
  EmailIcon,
  FacebookIcon,
  InstapaperIcon,
  LinkedinIcon,
  PocketIcon,
  RedditIcon,
  TelegramIcon,
  TwitterIcon,
  WhatsappIcon
} from "react-share";
import { Grid } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  sharebutton: {
    "&:focus": {
      outline: "none"
    }
  },
  shareicon: {
    margin: theme.spacing(1)
  }
}));

export default function ShareModal(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      {props.card ? (
        <Button size="small" color="primary" onClick={handleOpen}>
          <ShareIcon className={props.classes.actionIcon} /> Share
        </Button>
      ) : (
        <Button
          color="primary"
          onClick={handleOpen}
          className={props.classes.shareButton}
          variant="contained"
        >
          <ShareIcon />
        </Button>
      )}
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500
        }}
      >
        <Fade in={open}>
          {/* <div className={classes.paper}> */}
          <>
            <Grid container justify="center">
              <Grid item md={1} sm={2} xs={4}>
                <EmailShareButton className={classes.sharebutton}>
                  <EmailIcon
                    size={64}
                    round={true}
                    className={classes.shareicon}
                  />
                </EmailShareButton>
              </Grid>
              <Grid item md={1} sm={2} xs={4}>
                <FacebookShareButton url="#" className={classes.sharebutton}>
                  <FacebookIcon
                    size={64}
                    round={true}
                    className={classes.shareicon}
                  />
                </FacebookShareButton>
              </Grid>
              <Grid item md={1} sm={2} xs={4}>
                <InstapaperShareButton url="#" className={classes.sharebutton}>
                  <InstapaperIcon
                    size={64}
                    round={true}
                    className={classes.shareicon}
                  />
                </InstapaperShareButton>
              </Grid>
              <Grid item md={1} sm={2} xs={4}>
                <LinkedinShareButton url="#" className={classes.sharebutton}>
                  <LinkedinIcon
                    size={64}
                    round={true}
                    className={classes.shareicon}
                  />
                </LinkedinShareButton>
              </Grid>
              <Grid item md={1} sm={2} xs={4}>
                <PocketShareButton url="#" className={classes.sharebutton}>
                  <PocketIcon
                    size={64}
                    round={true}
                    className={classes.shareicon}
                  />
                </PocketShareButton>
              </Grid>
              <Grid item md={1} sm={2} xs={4}>
                <RedditShareButton url="#" className={classes.sharebutton}>
                  <RedditIcon
                    size={64}
                    round={true}
                    className={classes.shareicon}
                  />
                </RedditShareButton>
              </Grid>
              <Grid item md={1} sm={2} xs={4}>
                <TelegramShareButton url="#" className={classes.sharebutton}>
                  <TelegramIcon
                    size={64}
                    round={true}
                    className={classes.shareicon}
                  />
                </TelegramShareButton>
              </Grid>
              <Grid item md={1} sm={2} xs={4}>
                <TwitterShareButton url="#" className={classes.sharebutton}>
                  <TwitterIcon
                    size={64}
                    round={true}
                    className={classes.shareicon}
                  />
                </TwitterShareButton>
              </Grid>
              <Grid item md={1} sm={2} xs={4}>
                <WhatsappShareButton url="#" className={classes.sharebutton}>
                  <WhatsappIcon
                    size={64}
                    round={true}
                    className={classes.shareicon}
                  />
                </WhatsappShareButton>
              </Grid>
            </Grid>
            {/* <h2 id="transition-modal-title">Transition modal</h2>
            <p id="transition-modal-description">
              react-transition-group animates me.
            </p> */}
          </>
          {/* </div> */}
        </Fade>
      </Modal>
    </div>
  );
}
