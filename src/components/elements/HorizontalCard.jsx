import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import ShareIcon from "@material-ui/icons/Share";

const useStyles = makeStyles(theme => ({
  root: {
    position: "relative",
    background: "wheat",
    minWidth: "80%",
    height: theme.spacing(25),
    margin: theme.spacing(3),
    borderRadius: theme.spacing(1.5)
  },
  media: {
    height: theme.spacing(25),
    width: theme.spacing(25),
    borderRadius: `${theme.spacing(1.5)}px ${theme.spacing(
      0
    )}px ${theme.spacing(0)}px ${theme.spacing(1.5)}px`
  },
  content: {
    padding: theme.spacing(1)
  },
  action: {
    position: "absolute",
    bottom: theme.spacing(1),
    right: theme.spacing(1)
  }
}));

export default function HorizontalCard(props) {
  const classes = useStyles();
  return (
    <Paper className={classes.root}>
      <Grid container>
        <Grid item md={3} xs={5}>
          <img
            className={classes.media}
            src="https://images.unsplash.com/photo-1569429210811-641dced2167e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
            title="Contemplative Reptile"
            alt="test"
          />
        </Grid>

        <Grid item md={9} xs={7}>
          <div>
            <div className={classes.content}>
              May 30, 2019 - The image CSS data type represents a
              two-dimensional image. There are two kinds of images: plain
              images, referenced with a url, and ...May 30, 2019 - The image CSS
              data type represents a two-dimensional image. There are two kinds
              of images: plain images, referenced with a url, and ...
            </div>
            <div className={classes.action}>
              <IconButton>
                <ShareIcon /> SHARE
              </IconButton>
            </div>
          </div>
        </Grid>
      </Grid>
    </Paper>
  );
}
