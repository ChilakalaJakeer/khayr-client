import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

import Auth from "./page.auth/Auth";
import NavItems from "./NavItems";
import RouteLink from "./elements/RouteLink";

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    display: "none",
    [theme.breakpoints.up("xs")]: {
      display: "block"
    },
    color: theme.palette.primary.contrastText
  },
  icon: {
    color: theme.palette.primary.contrastText,
    marginRight: theme.spacing(3)
  },
  login: {
    color: "white"
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "flex"
    }
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("sm")]: {
      display: "none"
    }
  }
}));

export default function Navbar() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = event => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  const menuId = "primary-search-account-menu";

  const mobileMenuId = "primary-search-account-menu-mobile";

  return (
    <div className={classes.grow}>
      <AppBar position="static">
        <Toolbar>
          <RouteLink to="/">
            <Typography
              className={classes.title}
              variant="h6"
              component="h1"
              noWrap
            >
              Khayr
            </Typography>
          </RouteLink>

          <div className={classes.grow} />
          <NavItems
            pass={[
              classes,
              menuId,
              handleMobileMenuClose,
              handleMobileMenuOpen,
              mobileMoreAnchorEl,
              handleProfileMenuOpen,
              mobileMenuId,
              isMobileMenuOpen
            ]}
          />
        </Toolbar>
      </AppBar>
      {/* for mobile mode */}

      <Auth pass={[anchorEl, menuId, isMenuOpen, handleMenuClose]} />
    </div>
  );
}
