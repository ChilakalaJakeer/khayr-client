import React, { useState } from "react";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import makeStyles from "@material-ui/core/styles/makeStyles";
import { useMutation } from "@apollo/react-hooks";
import { addFundRaiserMutation } from "../../queries/graphql.queries";
import {
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio
} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  main: {
    width: "auto",
    display: "block", // Fix IE 11 issue.
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    [theme.breakpoints.up(400 + theme.spacing(3))]: {
      width: "80%",
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing(2)}px ${theme.spacing(3)}px ${theme.spacing(
      3
    )}px`,
    boxShadow: "5px 5px 10px"
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  cancel: {
    marginTop: theme.spacing(5)
  },
  submit: {
    backgroundColor: theme.palette.success.main,
    color: theme.palette.primary.contrastText,
    marginTop: theme.spacing(5),
    "&:hover": {
      backgroundColor: theme.palette.success.main,
      opacity: 0.9
    }
  }
}));

export default function CreateFundRaiser() {
  const classes = useStyles();

  const [Name, setName] = useState("");
  const [Age, setAge] = useState(0);
  const [Gender, setGender] = useState("Male");

  const [addFundRaiser, { data }] = useMutation(addFundRaiserMutation);
  //   const { from } = props.location.state || { from: { pathname: "/" } }; //has to test
  const submit = e => {
    e.preventDefault();
    Name &&
      Gender &&
      addFundRaiser({
        variables: {
          name: Name,
          age: Age,
          gender: Gender
        }
      });
    if (data) {
      return (window.location = "/");
    }
  };
  return (
    <main className={classes.main}>
      <Paper className={classes.paper}>
        {/* <Avatar className={classes.avatar}>
          <ListAltIcon />
        </Avatar> */}
        <Typography component="h1" variant="h5">
          Fundraiser
        </Typography>
        <form
          className={classes.form}
          onSubmit={e => e.preventDefault() && false}
        >
          <FormControl margin="normal" required fullWidth>
            <InputLabel htmlFor="name">Name</InputLabel>
            <Input
              id="name"
              name="name"
              autoComplete="off"
              autoFocus
              value={Name}
              onChange={e => setName(e.target.value)}
            />
          </FormControl>

          <TextField
            required
            fullWidth
            type="number"
            label="Age"
            id="aget"
            name="age"
            autoComplete="off"
            value={Age}
            onChange={e => setAge(parseInt(e.target.value))}
          />
          <FormControl fullWidth required>
            <FormLabel>Gender</FormLabel>
            <RadioGroup
              value={Gender}
              onChange={e => setGender(e.target.value)}
            >
              <FormControlLabel
                control={<Radio color="primary" />}
                label="Male"
                value="Male"
              />
              <FormControlLabel
                control={<Radio color="secondary" />}
                label="Female"
                value="Female"
              />
            </RadioGroup>
          </FormControl>

          <Grid container justify="center" spacing={3}>
            <Grid item md={5}>
              <Button
                type="submit"
                variant="contained"
                color="secondary"
                fullWidth
                // onClick={}
                className={classes.cancel}
              >
                Cancel
              </Button>
            </Grid>
            <Grid item md={5}>
              <Button
                type="submit"
                variant="contained"
                fullWidth
                onClick={submit}
                className={classes.submit}
              >
                Submit
              </Button>
            </Grid>
          </Grid>
        </form>
      </Paper>
    </main>
  );
}
