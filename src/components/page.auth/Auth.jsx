import React from "react";

import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import IconButton from "@material-ui/core/IconButton";
import PersonIcon from "@material-ui/icons/Person";
import firebase from "../../utils/firebase";
import RouteLink from "../elements/RouteLink";

export default function Auth(props) {
  const [anchorEl, menuId, isMenuOpen, handleMenuClose] = props.pass;
  async function logout() {
    await firebase.logout();
    window.location = "/";
  }

  return (
    <>
      {firebase.getCurrentUsername() && (
        <Menu
          anchorEl={anchorEl}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          id={menuId}
          keepMounted
          transformOrigin={{ vertical: "top", horizontal: "right" }}
          open={isMenuOpen}
          onClose={handleMenuClose}
        >
          <MenuItem onClick={handleMenuClose}>
            <RouteLink to="/profile">
              <IconButton>
                <PersonIcon />
              </IconButton>
              Profile
            </RouteLink>
          </MenuItem>
          <MenuItem onClick={logout}>
            <IconButton>
              <ExitToAppIcon />
            </IconButton>
            Logout
          </MenuItem>
        </Menu>
      )}
    </>
  );
}
