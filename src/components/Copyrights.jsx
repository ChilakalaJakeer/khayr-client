import React from "react";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    borderRadius: theme.spacing(0),
    position: "fixed",
    bottom: theme.spacing(0),
    width: "100%",
    textAlign: "center",
    backgroundColor: "slategray",
    paddingTop: theme.spacing(0.5),
    paddingBottom: theme.spacing(0.5)
  },
  typography: {
    color: "#fff",
    fontSize: 16
  }
}));

export default function Copyrights(props) {
  const classes = useStyles();
  return (
    <Paper color="secondary" className={classes.root}>
      <Typography variant="p" className={classes.typography}>
        &copy; Khayr 2020
      </Typography>
    </Paper>
  );
}
